/******************************************************************************
 * tdm (header) - Two Dimensional Matrix (header). A statically-allocated 2D
 *                matrix.
 * Fri Jun 17 16:15:11 BST 2022
 * Copyright (C) 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of tdm.
 *
 * tdm is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * tdm is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with tdm; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2022-06-17 Initial creation.
 * 2022-06-18 Simple operations.
 *            License under the LGPL.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>

#ifndef __TDM_H__
#define __TDM_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define TDM_MATRIX_DEF(NAME, ROWS, COLS) \
    tdm_t NAME = { 0 }; \
    double NAME ##_data[ROWS][COLS] = {{ 0.0 }}; \
    NAME.data = (double *)NAME ##_data; \
    NAME.rows = (sizeof(NAME ##_data) / sizeof (NAME ##_data[0])); \
    NAME.cols = (sizeof(NAME ##_data[0]) / sizeof(NAME ##_data[0][0]));

#define TDM_MATRIX_DEF_INIT(NAME, INIT) \
    tdm_t NAME = { 0 }; \
    NAME.data = (double *)INIT; \
    NAME.rows = (sizeof(INIT) / sizeof (INIT[0])); \
    NAME.cols = (sizeof(INIT[0]) / sizeof(INIT[0][0]));

typedef struct
{
    double *data;
    size_t rows;
    size_t cols;
}
tdm_t;

double *tdm_data_get(tdm_t *tdm);
size_t tdm_rows_get(tdm_t *tdm);
size_t tdm_cols_get(tdm_t *tdm);

int32_t tdm_get(tdm_t *tdm, int32_t row, int32_t col, double *output);

/******************************************************************************
 * Simple operations.
 ******************************************************************************/

/*
 * Copy the contents of source into dest.
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_copy(tdm_t *source, tdm_t *dest);

/*
 * Add source1 and source2, storing the result in dest.
 * dest = source1 + source2
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_add(tdm_t *source1, tdm_t *source2, tdm_t *dest);

/*
 * Subtract source2 from source1, storing the result in dest.
 * dest = source1 - source2
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_sub(tdm_t *source1, tdm_t *source2, tdm_t *dest);

/*
 * Transpose source into dest.
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_transpose(tdm_t *source, tdm_t *dest);

/*
 * Multiply source1 by source2, storing the result in dest.
 * dest = source1 * source2
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_mul(tdm_t *source1, tdm_t *source2, tdm_t *dest);

/*
 * Calculate the trace (spur) of a square matrix.
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_trace(tdm_t *source, double *trace);


#ifdef __cplusplus
}
#endif

#endif /* __TDM_H__ */

