/******************************************************************************
 * tdm - Two Dimensional Matrix. A statically-allocated 2D matrix.
 * Fri Jun 17 16:15:11 BST 2022
 * Copyright (C) 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of tdm.
 *
 * tdm is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * tdm is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with tdm; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2022-06-17 Initial creation.
 * 2022-06-18 Simple operations.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <malloc.h>
#include <stdlib.h>
#include <string.h>
#include "timber.h"
#include "mal.h"
#include "tdm.h"

#define NO_OF_OBJECTS   1024

#ifndef TESTING
int main(int argc, char *argv[])
{
    int32_t nspare;
    int32_t i;

    for (i = 0; i < argc; i++)
    {
        printf("%d : %s\n", i, argv[i]);
    }

    TDR_TIME_INIT();
    MAL_OPEN(NO_OF_OBJECTS);

    nspare = MAL_CLOSE();
    printf("Number of unreclaimed pointers: %d\n", nspare);
    return 0;
}
#endif

/*
 * No checking. For internal use only.
 */
static double retrieve(tdm_t *tdm, int32_t row, int32_t col)
{
    return *(tdm->data + (row * tdm->cols) + col);
}

/*
 * No checking. For internal use only.
 */
static void store(tdm_t *tdm, int32_t row, int32_t col, double data)
{
    *(double *)(tdm->data + (row * tdm->cols) + col) = data;
}


double *tdm_data_get(tdm_t *tdm)
{
    return tdm->data;
}

size_t tdm_rows_get(tdm_t *tdm)
{
    return tdm->rows;
}

size_t tdm_cols_get(tdm_t *tdm)
{
    return tdm->cols;
}

int32_t tdm_get(tdm_t *tdm, int32_t row, int32_t col, double *output)
{
    int32_t retcode = -1;

    if (NULL == tdm->data)
    {
    }
    else if (row < 0)
    {
    }
    else if (col < 0)
    {
    }
    else if (row >= tdm->rows)
    {
    }
    else if (col >= tdm->cols)
    {
    }
    else
    {
        *output = retrieve(tdm, row, col);
        retcode = 0;
    }

    return retcode;
}

/*
 * Copy the contents of source into dest.
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_copy(tdm_t *source, tdm_t *dest)
{
    int32_t retcode = -1;

    if (NULL == source)
    {
    }
    else if (NULL == dest)
    {
    }
    else if (dest->rows != source->rows)
    {
    }
    else
    {
        memcpy(dest, source, (size_t)(dest->rows * dest->cols * sizeof(double)));
        retcode = 0;
    }

    return retcode;
}

/*
 * Unit test interface.
 */
void tdm_test_tdm_zero(char *file, int32_t line, char *message, tdm_t *test)
{
    int32_t retcode;
	int32_t row;
	int32_t col;
	double output;

    if(NULL == file)
    {
        printf("tdm_test_tdm_zero(): NULL pointer to file.\n");
        exit(EXIT_FAILURE);
    }
    else if(NULL == message)
    {
        printf("tdm_test_tdm_zero(): NULL pointer to message.\n");
        exit(EXIT_FAILURE);
    }
    else if(NULL == test)
    {
        printf("tdm_test_tdm_zero(): NULL pointer to test.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        for (row = 0; row < test->rows; row++)
        {
            for (col = 0; col < test->cols; col++)
            {
                retcode = tdm_get(test, row, col, &output);
                if (0 != retcode)
                {
                    printf("%s (%d) %s retcode expected: %d, observed: %d.\n",
                           file, line, message, 0, retcode);
                    exit(EXIT_FAILURE);
                }
                if (0.0 != output)
                {
                    printf("%s (%d) %s expected: %f, observed: %f.\n",
                           file, line, message, 0.0, output);
                    exit(EXIT_FAILURE);
                }
            }
        }
    }
}

void tdm_test_tdm_equal(char *file, int32_t line, char *message, tdm_t *condition, tdm_t *test)
{
    int32_t retcode;
	int32_t row;
	int32_t col;
	double expected;
	double output;

    if(NULL == file)
    {
        printf("tdm_test_tdm_zero(): NULL pointer to file.\n");
        exit(EXIT_FAILURE);
    }
    else if(NULL == message)
    {
        printf("tdm_test_tdm_zero(): NULL pointer to message.\n");
        exit(EXIT_FAILURE);
    }
    else if(NULL == condition)
    {
        printf("tdm_test_tdm_zero(): NULL pointer to condition.\n");
        exit(EXIT_FAILURE);
    }
    else if(NULL == test)
    {
        printf("tdm_test_tdm_zero(): NULL pointer to test.\n");
        exit(EXIT_FAILURE);
    }
    else
    {
        for (row = 0; row < test->rows; row++)
        {
            for (col = 0; col < test->cols; col++)
            {
                retcode = tdm_get(condition, row, col, &expected);
                if (0 != retcode)
                {
                    printf("%s (%d) %s retcode expected: %d, observed: %d.\n",
                           file, line, message, 0, retcode);
                    exit(EXIT_FAILURE);
                }
                retcode = tdm_get(test, row, col, &output);
                if (0 != retcode)
                {
                    printf("%s (%d) %s retcode expected: %d, observed: %d.\n",
                           file, line, message, 0, retcode);
                    exit(EXIT_FAILURE);
                }
                if (output != expected)
                {
                    printf("%s (%d) %s expected: %f, observed: %f.\n",
                           file, line, message, expected, output);
                    exit(EXIT_FAILURE);
                }
#if 0
                printf("expected: %f, observed: %f\n", expected, output);
#endif
            }
        }
    }
}

/*
 * Add source1 and source2, storing the result in dest.
 * dest = source1 + source2
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_add(tdm_t *source1, tdm_t *source2, tdm_t *dest)
{
    int32_t retcode = -1;
    int32_t row;
    int32_t col;

    if (NULL == source1)
    {
    }
    else if (NULL == source2)
    {
    }
    else if (NULL == dest)
    {
    }
    else if (source1->rows != source2->rows)
    {
    }
    else if (source1->rows != dest->rows)
    {
    }
    else if (source1->cols != source2->cols)
    {
    }
    else if (source1->cols != dest->cols)
    {
    }
    else
    {
        for (row = 0; row < dest->rows; row++)
        {
            for (col = 0; col < dest->cols; col++)
            {
                store(dest, row, col, retrieve(source1, row, col) + retrieve(source2, row, col));
            }
        }
        retcode = 0;
    }

    return retcode;
}

/*
 * Subtract source2 from source1, storing the result in dest.
 * dest = source1 - source2
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_sub(tdm_t *source1, tdm_t *source2, tdm_t *dest)
{
    int32_t retcode = -1;
    int32_t row;
    int32_t col;

    if (NULL == source1)
    {
    }
    else if (NULL == source2)
    {
    }
    else if (NULL == dest)
    {
    }
    else if (source1->rows != source2->rows)
    {
    }
    else if (source1->rows != dest->rows)
    {
    }
    else if (source1->cols != source2->cols)
    {
    }
    else if (source1->cols != dest->cols)
    {
    }
    else
    {
        for (row = 0; row < dest->rows; row++)
        {
            for (col = 0; col < dest->cols; col++)
            {
                store(dest, row, col, retrieve(source1, row, col) - retrieve(source2, row, col));
            }
        }
        retcode = 0;
    }

    return retcode;
}

/*
 * Transpose source into dest.
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_transpose(tdm_t *source, tdm_t *dest)
{
    int32_t retcode = -1;
    int32_t row;
    int32_t col;

    if (NULL == source)
    {
    }
    else if (NULL == dest)
    {
    }
    else if (dest->rows != source->cols)
    {
    }
    else if (dest->cols != source->rows)
    {
    }
    else
    {
        for (row = 0; row < source->rows; row++)
        {
            for (col = 0; col < source->cols; col++)
            {
                store(dest, col, row, retrieve(source, row, col));
            }
        }
        retcode = 0;
    }

    return retcode;
}

/*
 * Multiply source1 by source2, storing the result in dest.
 * dest = source1 * source2
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_mul(tdm_t *source1, tdm_t *source2, tdm_t *dest)
{
    int32_t retcode = -1;
    int32_t row;
    int32_t col;
    int32_t index;
    double accum;

    if (NULL == source1)
    {
    }
    else if (NULL == source2)
    {
    }
    else if (NULL == dest)
    {
    }
    else if (source1->cols != source2->rows)
    {
    }
    else if (source1->cols != dest->rows)
    {
    }
    else
    {
        for (row = 0; row < source1->rows; row++)
        {
            for (col = 0; col < dest->cols; col++)
            {
                accum = 0.0;
                for (index = 0; index < dest->rows; index++)
                {
                    accum += retrieve(source1, row, index) * retrieve(source2, index, col);
                }
                store(dest, row, col, accum);
            }
        }
        retcode = 0;
    }
    return retcode;
}

/*
 * Calculate the trace (spur) of a square matrix.
 * Returns 0 on success, non-zero on failure.
 */
int32_t tdm_trace(tdm_t *source, double *trace)
{
    int32_t retcode = -1;
    int32_t row;
    double accum;

    if (NULL == source)
    {
    }
    else if (NULL == trace)
    {
    }
    else if (source->rows != source->cols)
    {
    }
    else
    {
        accum = 0.0;
        for (row = 0; row < source->rows; row++)
        {
            accum += retrieve(source, row, row);
        }
        *trace = accum;
        retcode = 0;
    }

    return retcode;
}

