#
# Makefile for tdm - Two Dimensional Matrix.
# Fri Jun 17 16:15:11 BST 2022
#
# Copyright (C) 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
# 
# This file is part of tdm.
#
# tdm is free software; you can redistribute it and/or modify it
# under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation; either version 2.1 of the License, or
# (at your option) any later version.
#
# tdm is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
# License for more details.
#
# You should have received a copy of the GNU Lesser General Public
# License along with tdm; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
# USA
#
# Modification history:
# 2022-06-17 Initial creation.
# 2022-06-18 License under the LGPL.
#

CC=gcc
CFLAGS=-fPIC -Wall -Werror -O
SHAREDFLAGS=-shared
CPPTESTFLAGS=-DTESTING
INCLUDES=-I/usr/local/include
LIBS=-lcutl -lmal -ltimber
INSTALL_DIR=/usr/local
INSTALL_LIB=$(INSTALL_DIR)/lib64
INSTALL_INC=$(INSTALL_DIR)/include

all: main test shared

test: tdm_test
	./tdm_test

main: tdm

tdm: tdm.h tdm.c
	$(CC) $(CFLAGS) $(INCLUDES) -c tdm.c
	$(CC) $(CFLAGS) -o tdm tdm.o $(LIBS)

tdm_test: tdm_test.c tdm.c tdm.h
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -DTESTSTUBS $(INCLUDES) -o tdm_testing.o -c tdm.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) $(INCLUDES) -c tdm_test.c
	$(CC) $(CFLAGS) $(CPPTESTFLAGS) -o tdm_test tdm_test.o tdm_testing.o $(LIBS)

shared: tdm
	$(CC) $(SHAREDFLAGS) -o libtdm.so tdm.o

install:
	mkdir -p $(INSTALL_INC)
	cp tdm.h $(INSTALL_INC)
	cp tdm_unit_test.h $(INSTALL_INC)
	mkdir -p $(INSTALL_LIB)
	cp libtdm.so $(INSTALL_LIB)

uninstall:
	rm -f $(INSTALL_INC)/tdm.h
	rm -f $(INSTALL_INC)/tdm_unit_test.h
	mkdir -p $(INSTALL_LIB)
	rm -f $(INSTALL_LIB)/libtdm.so

clean:
	rm -f *.o
	rm -f *.so
	rm -f tdm_test
	rm -f tdm

