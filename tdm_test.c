/******************************************************************************
 * tdm (test) - Two Dimensional Matrix (test). A statically-allocated 2D matrix.
 * Fri Jun 17 16:15:11 BST 2022
 * Copyright (C) 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of tdm.
 *
 * tdm is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * tdm is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with tdm; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 * 
 * Modification history:
 * 2022-06-17 Initial creation.
 * 2022-06-18 Simple operations.
 *            License under the LGPL.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include <assert.h>
#include "tdm.h"
#include "cutl.h"
#include "mal.h"
#include "tdm_unit_test.h"

void test_tdm_empty(void);
void test_tdm_scalar(void);
void test_tdm_row_vector(void);
void test_tdm_col_vector(void);
void test_tdm_matrix(void);
void test_tdm_scalar_bounds(void);
void test_tdm_row_vector_bounds(void);
void test_tdm_matrix_macro(void);
void test_tdm_copy(void);
void test_tdm_copy_mismatched(void);
void test_tdm_add(void);
void test_tdm_sub(void);
void test_tdm_transpose(void);
void test_tdm_mul(void);
void test_tdm_transpose_mismatched(void);
void test_tdm_mul_mismatched(void);
void test_tdm_trace(void);
void test_tdm_trace_mismatched(void);

cutl_test_case_t tests[] =
{
    { "tdm_empty",                test_tdm_empty                },
    { "tdm_scalar",               test_tdm_scalar               },
    { "tdm_row_vector",           test_tdm_row_vector           },
    { "tdm_col_vector",           test_tdm_col_vector           },
    { "tdm_matrix",               test_tdm_matrix               },
    { "tdm_scalar_bounds",        test_tdm_scalar_bounds        },
    { "tdm_row_vector_bounds",    test_tdm_row_vector_bounds    },
    { "tdm_matrix_macro",         test_tdm_matrix_macro         },
    { "tdm_copy",                 test_tdm_copy                 },
    { "tdm_copy_mismatched",      test_tdm_copy_mismatched      },
    { "tdm_add",                  test_tdm_add                  },
    { "tdm_sub",                  test_tdm_sub                  },
    { "tdm_transpose",            test_tdm_transpose            },
    { "tdm_mul",                  test_tdm_mul                  },
    { "tdm_transpose_mismatched", test_tdm_transpose_mismatched },
    { "tdm_mul_mismatched",       test_tdm_mul_mismatched       },
    { "tdm_trace",                test_tdm_trace                },
    { "tdm_trace_mismatched",     test_tdm_trace_mismatched     },
};

void tdm_test_case_setup(void);
void tdm_test_case_teardown(void);

#define NO_OF_OBJECTS   1024

void tdm_test_case_setup(void)
{
    MAL_OPEN(NO_OF_OBJECTS);
    cutl_stdlib_test_stubs_reset();
}

void tdm_test_case_teardown(void)
{
    int32_t nspare;

    nspare = MAL_CLOSE();
    if (0 != nspare)
    {
        printf("Number of unreclaimed pointers: %d\n", nspare);
    }
}

int main(int argc, char *argv[])
{
    printf("tdm test.\n");

    cutl_set_test_lib("tdm", tests, NO_OF_ELEMENTS(tests));
    cutl_set_cutl_test_setup(tdm_test_case_setup);
    cutl_set_cutl_test_teardown(tdm_test_case_teardown);
    cutl_get_options(argc, argv);
    cutl_run_tests();

    return 0;
}

void test_tdm_empty(void)
{
    tdm_t A = { 0 };
    int32_t retcode;
    double output = 1.0;

    TEST_PTR_NULL("data", tdm_data_get(&A));
    TEST_INT32_ZERO("rows", tdm_rows_get(&A));
    TEST_INT32_ZERO("cols", tdm_cols_get(&A));

    retcode = tdm_get(&A, 0, 0, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 1.0, output);
}

void test_tdm_scalar(void)
{
    tdm_t A = { 0 };
    double A_data[1][1] = {{ 1.0 }};
    int32_t retcode;
    double output = 0.0;

    A.data = (double *)A_data;
    A.rows = (sizeof(A_data) / sizeof(A_data[0]));
    A.cols = (sizeof(A_data[0]) / sizeof(A_data[0][0]));

    TEST_PTR_NOT_NULL("data", tdm_data_get(&A));
    TEST_INT32_EQUAL("rows", 1, tdm_rows_get(&A));
    TEST_INT32_EQUAL("cols", 1, tdm_cols_get(&A));

    retcode = tdm_get(&A, 0, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output", 1.0, output);
}

void test_tdm_row_vector(void)
{
    tdm_t A = { 0 };
    double A_data[1][4] = {{ 1.0, 2.0, 3.0, 4.0 }};
    int32_t retcode;
    double output;

    A.data = (double *)A_data;
    A.rows = (sizeof(A_data) / sizeof(A_data[0]));
    A.cols = (sizeof(A_data[0]) / sizeof(A_data[0][0]));

    TEST_PTR_NOT_NULL("data", tdm_data_get(&A));
    TEST_INT32_EQUAL("rows", 1, tdm_rows_get(&A));
    TEST_INT32_EQUAL("cols", 4, tdm_cols_get(&A));

    retcode = tdm_get(&A, 0, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 1.0, output);
    
    retcode = tdm_get(&A, 0, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 2.0, output);

    retcode = tdm_get(&A, 0, 2, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 3.0, output);

    retcode = tdm_get(&A, 0, 3, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [3]", 4.0, output);
}

void test_tdm_col_vector(void)
{
    tdm_t A = { 0 };
    double A_data[4][1] = {{ 1.0 }, { 2.0 }, { 3.0 }, {4.0}};
    int32_t retcode;
    double output;

    A.data = (double *)A_data;
    A.rows = (sizeof(A_data) / sizeof(A_data[0]));
    A.cols = (sizeof(A_data[0]) / sizeof(A_data[0][0]));

    TEST_PTR_NOT_NULL("data", tdm_data_get(&A));
    TEST_INT32_EQUAL("rows", 4, tdm_rows_get(&A));
    TEST_INT32_EQUAL("cols", 1, tdm_cols_get(&A));

    retcode = tdm_get(&A, 0, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 1.0, output);

    retcode = tdm_get(&A, 1, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 2.0, output);

    retcode = tdm_get(&A, 2, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 3.0, output);

    retcode = tdm_get(&A, 3, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [3]", 4.0, output);
}

void test_tdm_matrix(void)
{
    tdm_t A = { 0 };
    double A_data[3][3] = {{ 1.0, 2.0, 3.0 },
                           { 4.0, 5.0, 6.0 },
                           { 7.0, 8.0, 9.0 }};
    int32_t retcode;
    double output;

    A.data = (double *)A_data;
    A.rows = (sizeof(A_data) / sizeof(A_data[0]));
    A.cols = (sizeof(A_data[0]) / sizeof(A_data[0][0]));

    TEST_PTR_NOT_NULL("data", tdm_data_get(&A));
    TEST_INT32_EQUAL("rows", 3, tdm_rows_get(&A));
    TEST_INT32_EQUAL("cols", 3, tdm_cols_get(&A));

    retcode = tdm_get(&A, 0, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 1.0, output);
    
    retcode = tdm_get(&A, 0, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 2.0, output);

    retcode = tdm_get(&A, 0, 2, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 3.0, output);

    retcode = tdm_get(&A, 1, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 4.0, output);
   
    retcode = tdm_get(&A, 1, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 5.0, output);

    retcode = tdm_get(&A, 1, 2, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 6.0, output);

    retcode = tdm_get(&A, 2, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 7.0, output);
   
    retcode = tdm_get(&A, 2, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 8.0, output);

    retcode = tdm_get(&A, 2, 2, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 9.0, output);

}

void test_tdm_scalar_bounds(void)
{
    tdm_t A = { 0 };
    double A_data[1][1] = {{ 1.0 }};
    int32_t retcode;
    double output = 0.0;

    A.data = (double *)A_data;
    A.rows = (sizeof(A_data) / sizeof(A_data[0]));
    A.cols = (sizeof(A_data[0]) / sizeof(A_data[0][0]));

    TEST_PTR_NOT_NULL("data", tdm_data_get(&A));
    TEST_INT32_EQUAL("rows", 1, tdm_rows_get(&A));
    TEST_INT32_EQUAL("cols", 1, tdm_cols_get(&A));

    retcode = tdm_get(&A, -1, 0, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 0.0, output);

    retcode = tdm_get(&A, 0, -1, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 0.0, output);

    retcode = tdm_get(&A, -1, -1, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 0.0, output);

    retcode = tdm_get(&A, 1, 0, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 0.0, output);

    retcode = tdm_get(&A, 0, 1, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 0.0, output);
}

void test_tdm_row_vector_bounds(void)
{
    tdm_t A = { 0 };
    double A_data[1][4] = {{ 1.0, 2.0, 3.0, 4.0 }};
    int32_t retcode;
    double output = 10.0;

    A.data = (double *)A_data;
    A.rows = (sizeof(A_data) / sizeof(A_data[0]));
    A.cols = (sizeof(A_data[0]) / sizeof(A_data[0][0]));

    TEST_PTR_NOT_NULL("data", tdm_data_get(&A));
    TEST_INT32_EQUAL("rows", 1, tdm_rows_get(&A));
    TEST_INT32_EQUAL("cols", 4, tdm_cols_get(&A));

    retcode = tdm_get(&A, -1, -1, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 10.0, output);

    retcode = tdm_get(&A, -1, 5, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 10.0, output);

    retcode = tdm_get(&A, -1, 0, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 10.0, output);
    
    retcode = tdm_get(&A, -1, 4, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 10.0, output);
 
    retcode = tdm_get(&A, 0, -1, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 10.0, output);

    retcode = tdm_get(&A, 0, 5, &output);
    TEST_INT32_EQUAL("retcode", -1, retcode);
    TEST_DOUBLE_EQUAL("output", 10.0, output);
}

void test_tdm_matrix_macro(void)
{
    double B_data[2][2] = {{ 1.0, 2.0 },
                           { 3.0, 4.0 }};
    int32_t retcode;
    double output;

    /*
     * Initialise a matrix with all zeros.
     */
    TDM_MATRIX_DEF(matrixA, 4, 4);

    TEST_PTR_NOT_NULL("data", tdm_data_get(&matrixA));
    TEST_INT32_EQUAL("rows", 4, tdm_rows_get(&matrixA));
    TEST_INT32_EQUAL("cols", 4, tdm_cols_get(&matrixA));

    retcode = tdm_get(&matrixA, 0, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 0.0, output);
    
    retcode = tdm_get(&matrixA, 0, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 0.0, output);

    retcode = tdm_get(&matrixA, 0, 2, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 0.0, output);

    retcode = tdm_get(&matrixA, 1, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 0.0, output);
   
    retcode = tdm_get(&matrixA, 1, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 0.0, output);

    retcode = tdm_get(&matrixA, 1, 2, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 0.0, output);

    retcode = tdm_get(&matrixA, 2, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0]", 0.0, output);
   
    retcode = tdm_get(&matrixA, 2, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1]", 0.0, output);

    retcode = tdm_get(&matrixA, 2, 2, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [2]", 0.0, output);

    /*
     * Initialise a matrix with a data array provided.
     */
    TDM_MATRIX_DEF_INIT(matrixB, B_data);

    TEST_PTR_NOT_NULL("data", tdm_data_get(&matrixB));
    TEST_INT32_EQUAL("rows", 2, tdm_rows_get(&matrixB));
    TEST_INT32_EQUAL("cols", 2, tdm_cols_get(&matrixB));

    retcode = tdm_get(&matrixB, 0, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0][0]", 1.0, output);
    
    retcode = tdm_get(&matrixB, 0, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [0][1]", 2.0, output);

    retcode = tdm_get(&matrixB, 1, 0, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1][0]", 3.0, output);

    retcode = tdm_get(&matrixB, 1, 1, &output);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("output [1][1]", 4.0, output);
}

void test_tdm_copy(void)
{
    double B_data[2][2] = {{ 1.0, 2.0 },
                           { 3.0, 4.0 }};
    int32_t retcode;

    /*
     * Initialise a matrix with all zeros.
     */
    TDM_MATRIX_DEF(matrixA, 2, 2);

    TEST_PTR_NOT_NULL("data", tdm_data_get(&matrixA));
    TEST_INT32_EQUAL("rows", 2, tdm_rows_get(&matrixA));
    TEST_INT32_EQUAL("cols", 2, tdm_cols_get(&matrixA));

    /*
     * Initialise a matrix with a data array provided.
     */
    TDM_MATRIX_DEF_INIT(matrixB, B_data);

    TEST_PTR_NOT_NULL("data", tdm_data_get(&matrixB));
    TEST_INT32_EQUAL("rows", 2, tdm_rows_get(&matrixB));
    TEST_INT32_EQUAL("cols", 2, tdm_cols_get(&matrixB));

    /*
     * Copy the data from B to A
     */
    retcode = tdm_copy(&matrixB, &matrixA);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    TDM_TEST_TDM_EQUAL("matrixA", &matrixB, &matrixA);
}

void test_tdm_copy_mismatched(void)
{
    double B_data[2][2] = {{ 1.0, 2.0 },
                           { 3.0, 4.0 }};
    int32_t retcode;

    /*
     * Initialise a matrix with all zeros.
     */
    TDM_MATRIX_DEF(matrixA, 3, 2);

    /*
     * Initialise another matrix with all zeros.
     */
    TDM_MATRIX_DEF(matrixC, 3, 2);

    /*
     * Initialise a matrix with a data array provided.
     */
    TDM_MATRIX_DEF_INIT(matrixB, B_data);

    /*
     * Attempt to copy the data from B to A
     */
    retcode = tdm_copy(&matrixB, &matrixA);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    TDM_TEST_TDM_ZERO("matrixA", &matrixA);
    TDM_TEST_TDM_EQUAL("matrixA", &matrixC, &matrixA);
}

/*
 * Add two 4x4 matrices.
 */
void test_tdm_add(void)
{
    double A_data[4][4] = {{  1.0,  2.0,  3.0,  4.0 },
                           {  5.0,  6.0,  7.0,  8.0 },
                           {  9.0, 10.0, 11.0, 12.0 },
                           { 13.0, 14.0, 15.0, 16.0 }};

    double B_data[4][4] = {{  100.0,  200.0,  300.0,  400.0 },
                           {  500.0,  600.0,  700.0,  800.0 },
                           {  900.0, 1000.0, 1100.0, 1200.0 },
                           { 1300.0, 1400.0, 1500.0, 1600.0 }};

    double E_data[4][4] = {{  101.0,  202.0,  303.0,  404.0 },
                           {  505.0,  606.0,  707.0,  808.0 },
                           {  909.0, 1010.0, 1111.0, 1212.0 },
                           { 1313.0, 1414.0, 1515.0, 1616.0 }};


    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF_INIT(matrixB, B_data);
    TDM_MATRIX_DEF_INIT(matrixE, E_data);
    TDM_MATRIX_DEF(matrixC, 4, 4);

    int32_t retcode;

    /*
     * Add A with B and store in C
     * C = A + B
     */
    retcode = tdm_add(&matrixA, &matrixB, &matrixC);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    TDM_TEST_TDM_EQUAL("add", &matrixE, &matrixC);
}

/*
 * Subtract two 4x4 matrices.
 */
void test_tdm_sub(void)
{
    double A_data[4][4] = {{  1.0,  2.0,  3.0,  4.0 },
                           {  5.0,  6.0,  7.0,  8.0 },
                           {  9.0, 10.0, 11.0, 12.0 },
                           { 13.0, 14.0, 15.0, 16.0 }};

    double B_data[4][4] = {{  100.0,  200.0,  300.0,  400.0 },
                           {  500.0,  600.0,  700.0,  800.0 },
                           {  900.0, 1000.0, 1100.0, 1200.0 },
                           { 1300.0, 1400.0, 1500.0, 1600.0 }};

    double E_data[4][4] = {{  101.0,  202.0,  303.0,  404.0 },
                           {  505.0,  606.0,  707.0,  808.0 },
                           {  909.0, 1010.0, 1111.0, 1212.0 },
                           { 1313.0, 1414.0, 1515.0, 1616.0 }};


    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF_INIT(matrixB, B_data);
    TDM_MATRIX_DEF_INIT(matrixE, E_data);
    TDM_MATRIX_DEF(matrixC, 4, 4);

    int32_t retcode;

    /*
     * Subtract B from E and store in C
     * C = E - B
     */
    retcode = tdm_sub(&matrixE, &matrixB, &matrixC);
    TEST_INT32_EQUAL("retcode", 0, retcode);

    TDM_TEST_TDM_EQUAL("sub", &matrixA, &matrixC);
}

void test_tdm_transpose(void)
{
    double A_data[2][3] = {{ 1.0, 2.0, 3.0 },
                           { 4.0, 5.0, 6.0 }};
    double E_data[3][2] = {{ 1.0, 4.0 },
                           { 2.0, 5.0 },
                           { 3.0, 6.0 }};
    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF_INIT(matrixE, E_data);
    TDM_MATRIX_DEF(matrixB, 3, 2);
    int32_t retcode;

    retcode = tdm_transpose(&matrixA, &matrixB);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TDM_TEST_TDM_EQUAL("transpose", &matrixE, &matrixB);

}

void test_tdm_mul(void)
{
    double A_data[2][2] = {{ 2.0, 3.0 },
                           { 4.0, 5.0 }};
    double B_data[2][2] = {{ 1.0, 10.0 },
                           { 100.0, 1000.0 }};
    double E_data[2][2] = {{ 302.0, 3020.0 },
                           { 504.0, 5040.0 }};
    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF_INIT(matrixB, B_data);
    TDM_MATRIX_DEF_INIT(matrixE, E_data);
    TDM_MATRIX_DEF(matrixC, 2, 2);
    int32_t retcode;

    retcode = tdm_mul(&matrixA, &matrixB, &matrixC);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TDM_TEST_TDM_EQUAL("mul", &matrixE, &matrixC);

}

void test_tdm_transpose_mismatched(void)
{
    double A_data[2][3] = {{ 1.0, 2.0, 3.0 },
                           { 4.0, 5.0, 6.0 }};
    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF(matrixB, 2, 2);
    TDM_MATRIX_DEF(matrixC, 2, 3);
    TDM_MATRIX_DEF(matrixD, 3, 3);
    int32_t retcode;

    retcode = tdm_transpose(&matrixA, &matrixB);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    retcode = tdm_transpose(&matrixA, &matrixC);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    retcode = tdm_transpose(&matrixA, &matrixD);
    TEST_INT32_EQUAL("retcode", -1, retcode);
}

void test_tdm_mul_mismatched(void)
{
    double A_data[2][2] = {{ 2.0, 3.0 },
                           { 4.0, 5.0 }};
    double B_data[2][2] = {{ 1.0, 10.0 },
                           { 100.0, 1000.0 }};
    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF_INIT(matrixB, B_data);
    TDM_MATRIX_DEF(matrixC, 3, 2);
    TDM_MATRIX_DEF(matrixD, 1, 2);
    int32_t retcode;

    retcode = tdm_mul(&matrixA, &matrixB, &matrixC);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    retcode = tdm_mul(&matrixA, &matrixD, &matrixC);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    /*
     * FIXME: Mental block.
     */
}

void test_tdm_trace(void)
{
    double A_data[2][2] = {{ 2.0, 3.0 },
                           { 4.0, 5.0 }};

    double B_data[3][3] = {{ 1.0, 2.0, 3.0 },
                           { 4.0, 5.0, 6.0 },
                           { 7.0, 8.0, 9.0 }};

    double C_data[4][4] = {{  1.0,  2.0,  3.0,  4.0 },
                           {  5.0,  6.0,  7.0,  8.0 },
                           {  9.0, 10.0, 11.0, 12.0 },
                           { 13.0, 14.0, 15.0, 16.0 }};

    double D_data[1][1] = { { 1.0 } };

    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF_INIT(matrixB, B_data);
    TDM_MATRIX_DEF_INIT(matrixC, C_data);
    TDM_MATRIX_DEF_INIT(matrixD, D_data);

    int32_t retcode;
    double trace;

    retcode = tdm_trace(&matrixA, &trace);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("trace", 7.0, trace);

    retcode = tdm_trace(&matrixB, &trace);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("trace", 15.0, trace);

    retcode = tdm_trace(&matrixC, &trace);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("trace", 34.0, trace);

    retcode = tdm_trace(&matrixD, &trace);
    TEST_INT32_EQUAL("retcode", 0, retcode);
    TEST_DOUBLE_EQUAL("trace", 1.0, trace);
}

void test_tdm_trace_mismatched(void)
{
    double A_data[2][1] = {{ 2.0 },
                           { 4.0 }};

    double B_data[2][3] = {{ 1.0, 2.0, 3.0 },
                           { 7.0, 8.0, 9.0 }};

    double C_data[4][3] = {{  1.0,  2.0,  4.0 },
                           {  5.0,  6.0,  8.0 },
                           {  9.0, 10.0, 12.0 },
                           { 13.0, 14.0, 16.0 }};

    TDM_MATRIX_DEF_INIT(matrixA, A_data);
    TDM_MATRIX_DEF_INIT(matrixB, B_data);
    TDM_MATRIX_DEF_INIT(matrixC, C_data);

    int32_t retcode;
    double trace;

    retcode = tdm_trace(&matrixA, &trace);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    retcode = tdm_trace(&matrixB, &trace);
    TEST_INT32_EQUAL("retcode", -1, retcode);

    retcode = tdm_trace(&matrixC, &trace);
    TEST_INT32_EQUAL("retcode", -1, retcode);
}

