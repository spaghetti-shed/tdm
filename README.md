tdm
===

TDM is a C library of very simple two-dimensional matrix operations which work
on statically-allocated matrices featuring bounds checking and test assertions.

  Copyright (C) 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
  
  This file is part of tdm.
 
  tdm is free software; you can redistribute it and/or modify it
  under the terms of the GNU Lesser General Public License as published
  by the Free Software Foundation; either version 2.1 of the License, or
  (at your option) any later version.
 
  tdm is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License for more details.
 
  You should have received a copy of the GNU Lesser General Public
  License along with tdm; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
  USA

Introduction
------------

TDM is a C library of very simple two-dimensional matrix operations which work
on statically-allocated matrices featuring bounds checking and test assertions.

It was written on Slackware Linux 15.0.

Why
---

Do you enjoy late nights spent stepping through code with a debugger? Do you enjoy strange, random errors that only occur intermittently? No? Neither do I. This was written to demonstrate an idea of applying some of the principles of Test Driven Development to a matrix library. Bugs are expensive. The fewer bugs you put into your code, the less expensive it is in terms of time and effort to get it working. The more time you can spend solving interesting problems instead of wondering whether there are extra bugs you haven't encountered yet. Programming is hard enough. Let the compiler and build system find your bugs.

Design
------

The design principles are:
* The matrices are statically-allocated and initialised at compile time.
* Error checking should be selectable at compile time, enabled by default for development, and eliminated (not compiled in) when the code has been tested and there is confidence it is correct.
* The API is pass-by-reference, ie pointers to the matrix structure, rather than using expensive copies on the stack. The dimensions of the matrix are stored in a structure which also holds a pointer to the matrix data.
* The matrix data is held in a plain C array and can be operated on using existing routines designed to use plain arrays.
* The dimensions of the matrix are either specified at definition time explicitly, or derived from the size of a pre-initialised data array.
* Execution of the program should stop immetiately when an error is detected. All matrix functions return an error code which is non-zero on error.
* The matrix elements are double-precision floating-point.
* Vectors and scalars are just special cases of matrix.

Building and Installing
-----------------------

tdm depends on cutl and mal which also depend on timber. You can download and install them manually.

1. https://gitlab.com/spaghetti-shed/timber.git
2. https://gitlab.com/spaghetti-shed/mal.git
3. https://gitlab.com/spaghetti-shed/cutl.git

To perform a manual install of tdm, the steps are:
Obtain the source from gitlab

    git clone https://gitlab.com/spaghetti-shed/tdm.git

In the tdm directory type 'make' to compile the code and run the unit tests

    cd tdm
    make

As root, type 'make install' to install the binaries and headers under /usr/local and update the dynamic linker cache

    su
    make install
    /sbin/ldconfig

For the adventurous, the cowboy builder (https://gitlab.com/spaghetti-shed/cowboy) can be used to download, build and install the dependencies automatically.

To build using the cowboy builder, and to download, build and install dependencies automatically (WARNING: runs make install as root):

    cowboy --dangerous tdm_project.cow


Use
---

Start by looking at the unit tests (tdm_test.c) for clues.

Essentially, there are two macros provided for defining arrays (see tdm.h):

    TDM_MATRIX_DEF(NAME, ROWS, COLS)

This defines an array of size ROWS x COLS initialised (compile time) to 0.0 and defines a container structure which holds the array dimensions and a pointer to the array.

For example:

    TDM_MATRIX_DEF(square, 4, 4);

This defines a 4x4 matrix called "square" (a structure of type tdm_t) with the row and column fields initialised, an array cleed square_data of dimension 4x4 initialised to 0.0 and initialises the data pointer in the structure to point to the data array.

    TDM_MATRIX_DEF_INIT(NAME, INIT)

This defines a matrix structure to contain (wrap) an existing static array of data.

For example, to create a pre-initialised 3-element row vector:

    vector_data[1][3] = { 1.0, 2.0, 3.0 };
    TDM_MATRIX_DEF_INIT(vector, vector_data);

And so on.

The functions return a code (int32_t) which is 0 on success or non-zero on failure.

Frequently Anticipated Questions
--------------------------------

Q1: It doesn't do very much, does it?  
A1: No. I started writing it on a Friday afternoon after work and worked some more on it on the Saturday and Sunday.

Q2: Why bother?  
A2: I wanted to try out some ideas for finding certain bugs in matrix code.

Q3: Shouldn't you be writing this in C++?  
A3: Wash your mouth out with soap and water.

Q4: Why didn't you just use package ${FOO}?  
A4: I needed some code to which I owned the copyright, so I wrote it from scratch.

Q5: Your code has some ideas I'd like to use but I can't because of your license.  
A5: Speak nicely to me.

Q6: Can it do matrices with more than two dimensions?  
A6: No.

Q7: Why not?  
A7: It doesn't need to.

Q8: Is it fast?  
A8: No idea. I don't even know how buggy it is yet.

TO-DO
-----

* More operations. It doesn't even do determinants!
* Optimisations.
* Compile-time macros to turn off dimension checks.
* Other data types.
* Performance comparisons (with and without dimension checks).

