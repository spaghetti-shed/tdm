/******************************************************************************
 * tdm_unit_test (header) - Two Dimensional Matrix unit test interface (header).
 * Sat Jun 18 09:21:14 BST 2022
 * Copyright (C) 2022 by Iain Nicholson. <iain.j.nicholson@gmail.com>
 *
 * This file is part of tdm.
 *
 * tdm is free software; you can redistribute it and/or modify it
 * under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation; either version 2.1 of the License, or
 * (at your option) any later version.
 *
 * tdm is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with tdm; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
 * USA
 *
 * Modification history:
 * 2022-06-18 Initial creation.
 ******************************************************************************/

#include <stdint.h>
#include <stdio.h>
#include "tdm.h"

#ifndef __TDM_UNIT_TEST_H__
#define __TDM_UNIT_TEST_H__

#ifdef __cplusplus
extern "C"
{
#endif

#define TDM_TEST_TDM_ZERO(message, test) tdm_test_tdm_zero(__FILE__, __LINE__, message, test)
#define TDM_TEST_TDM_EQUAL(message, condition, test) tdm_test_tdm_equal(__FILE__, __LINE__, message, condition, test)

/*
 * Unit test interface functions (assersions).
 */
void tdm_test_tdm_zero(char *file, int32_t line, char *message, tdm_t *test);
void tdm_test_tdm_equal(char *file, int32_t line, char *message, tdm_t *condition, tdm_t *test);


#ifdef __cplusplus
}
#endif

#endif /* __TDM_UNIT_TEST_H__ */

